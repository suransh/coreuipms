/**
 * Created by prabal on 15/6/17.
 */

import setAuthorizationToken from '../utils/setAuthorizationToken'
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import config from '../config';

export function setCurrentUser(user) {
    return {
        type: "SET_CURRENT_USER",
        user
    };
}

export function editCurrentUser(user) {
    return {
        type: "EDIT_CURRENT_USER",
        user
    };
}

export function logout() {
    return dispatch => {
        localStorage.removeItem('jwtToken');
        setAuthorizationToken(false);
        dispatch(setCurrentUser({}));
    }
}

export function login(data) {
    return dispatch => {
        return axios.post('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/auth', data).then(res => {
            const token = res.data.token;
            localStorage.setItem('jwtToken', token);
            setAuthorizationToken(token);
            dispatch(setCurrentUser(jwtDecode(token)));
        });
    }
}

export function editProfile(data) {
    return dispatch => {
        dispatch(editCurrentUser(data));
    }
}
