/**
 * Created by prabal on 10/7/17.
 */

export default  {
    APP_SERVER_IP: "localhost",
    APP_SERVER_PORT: "8080",
    admin: "Admin",
    superAdmin: "Super Admin",
    startYear: 2010
}
