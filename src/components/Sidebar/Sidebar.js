import React, { Component } from 'react';
import { NavLink,withRouter } from 'react-router-dom'
import {connect} from 'react-redux';
import config from '../../config';

class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName) {
      for (let i = 0;i<routeName.length ;i++) {
          if(this.props.location.pathname.indexOf(routeName[i]) > -1) return 'nav-item nav-dropdown open';
      }
      return  'nav-item nav-dropdown';
  }

  adminPanel() {
      const {role} = this.props.user;
      if (role===config.admin || role===config.superAdmin) {
          return (
              <div>
          <li className="nav-title">
              Admin Rights
          </li>
              <li className={this.activeRoute(["/admin/list","/admin/add","/admin/edit"])}>
                  <a className="nav-link nav-dropdown-toggle" onClick={this.handleClick.bind(this)}><i className="icon-people"/>Users</a>
                  <ul className="nav-dropdown-items">
                    <li className="nav-item">
                        <NavLink to='/admin/list' className="nav-link" activeClassName="active"><i className="icon-list"/> List</NavLink>
                    </li>
                    <li className="nav-item">
                         <NavLink to='/admin/add' className="nav-link" activeClassName="active"><i className="icon-plus"/> Add</NavLink>
                    </li>
                  </ul>
              </li>
                  <li className="nav-item">
                      <NavLink to="/admin/perfmanage" className="nav-link" activeClassName = 'active' >
                          <i className="icon-speedometer"/>Performance</NavLink>
                  </li>

              </div>
          )
      }
      return null;
  }

  render() {
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
              {this.adminPanel()}
          </ul>
        </nav>
      </div>
    )
  }
}

export default withRouter(connect((state) => ({ user: state.auth.user }),null)(Sidebar));
