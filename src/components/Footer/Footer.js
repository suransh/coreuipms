import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="http://www.successivesoftwares.com/" target="_blank" rel="noopener noreferrer">
            Successive Software</a> &copy;
      </footer>
    )
  }
}

export default Footer;
