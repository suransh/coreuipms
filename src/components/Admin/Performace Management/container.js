/**
 * Created by prabal on 14/7/17.
 */

import React from 'react';
import SelectEmp from './SelectEmp';

class Performance extends React.Component {


    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <strong><h5> Performance Management </h5></strong>
                </div>
                <div className="card-block">
                    <div className="col-md-12">
                        <SelectEmp/>
                    </div>
                </div>
            </div>
        )
    }

}

export default Performance;