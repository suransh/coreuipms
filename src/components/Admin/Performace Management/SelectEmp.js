import React from 'react';
import Select from 'react-select';
import axios from 'axios';
import config from '../../../config';
import Loading from 'react-loading-animation';
import currentweek from 'current-week-number';
import ScoreForm from './ScoreForm';
import validate from '../../../utils/ValidatingForms';

class Form extends React.Component {

    constructor() {
        super();
        this.state = {
            empData: [],
            isFetching: true,
            selectedId: null,
            selectedWeek: null,
            selectedYear: null,
            weeks: [],
            years: []
        }
    }

    componentWillMount() {
        const currYear = new Date().getFullYear();
        let years = [];
        for (let i = config.startYear;i<=currYear;i++) years.push({ value: i, label: i});

        this.setState({ years:years, selectedYear: currYear });

        this.weeksOptions(currYear);

        axios.get('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/perfmanage/getname').then( res => {
            let options = [];
            for (let i = 0; i<res.data.length ; i++ ) {
                if (res.data[i].role===config.admin || res.data[i].role===config.superAdmin) continue;
                let obj = {
                    value: res.data[i]._id,
                    label: res.data[i].firstName + ' ' + res.data[i].lastName
                };
                options.push(obj);
            }
        this.setState({ empData: options, isFetching: false });
    } );
    }

    render() {
        const {isFetching, selectedId, selectedWeek, selectedYear} = this.state;
        if (isFetching) return <Loading/>;
        return(
            <div>
                <form className="form form-inline col-md-12 offset-md-1 ">
                    <div className="col-md-10">
                        <label className="pull-left"><strong>Select Employee</strong></label>
                        <Select
                            value = {selectedId}
                            options = {this.state.empData}
                            onChange={this.onSelectEmp.bind(this)}
                        />
                        <br/>
                    </div>
                    <div className="col-md-5">
                        <label className="pull-left"><strong>Select Year</strong></label>
                        <Select
                            value = {selectedYear}
                            options = {this.state.years}
                            onChange = {this.onSelectYear.bind(this)}
                        />
                    </div>
                    <div className="col-md-5">
                    <label className="pull-left"><strong>Select Week</strong></label>
                    <Select
                        value = {selectedWeek}
                        options = {this.state.weeks}
                        onChange = {this.onSelectWeek.bind(this)}
                    />
                    </div>
                </form>
                <br/> <br/>
                {this.readyForForm() ?
                    <ScoreForm selectedId = {selectedId} selectedWeek = {selectedWeek} selectedYear = {selectedYear} />
                    : null}
            </div>
        )
    }

    onSelectEmp(e) {
        this.setState({ selectedId: e ? e.value : null});

    }

    onSelectWeek(e) {
        this.setState({ selectedWeek: e ? parseInt(e.value,10) : null });
    }

    onSelectYear(e) {
        this.setState({ selectedYear: e ? parseInt(e.value,10) : null });
        if (e) {
            this.weeksOptions(e.value);
        }
    }

    getDateOfISOWeek(w,y) {
        let simple = new Date(y, 0, 1 + (w - 1) * 7);
        let dow = simple.getDay();
        let ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }

    weeksOptions(y) {
        let weeks = [];
        for (let i = 1;i<=52;i++) {
            weeks.push({ value: i, label:
            "Week " + i + ' (' + this.getDateOfISOWeek(i,y).toISOString().split('T')[0] + ' to ' +
            this.getDateOfISOWeek( i+1<=52 ? i+1 : 1, i+1<=52 ? y : y+1 ).toISOString().split('T')[0] + ')' } )
        }
        this.setState({ weeks: weeks, selectedWeek: currentweek() });
    }

    readyForForm() {
        const {isValid} = validate(this.state);
        return isValid
    }

}

export default Form;