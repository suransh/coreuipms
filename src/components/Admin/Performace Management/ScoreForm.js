import React from 'react';
import axios from 'axios';
import config from '../../../config';
import Loading from 'react-loading-animation';
import classnames from 'classname';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';

class ScoreForm extends React.Component {

    constructor() {
        super();
        this.state = {
            categories: [],
            isFetching: true,
            score: {},
            maxScore: {},
            error: {},
            isLoading: false,
            showSuccess: false
        }
    }

    componentDidMount() {
        const {selectedId,selectedWeek,selectedYear} = this.props;
        this.fetchData(selectedId,selectedWeek,selectedYear);
    }

    componentWillReceiveProps(nextProps) {
        const {selectedId,selectedWeek,selectedYear} = nextProps;
        this.fetchData(selectedId,selectedWeek,selectedYear);
    }

    render() {
        const {isFetching,categories,score,isLoading,error} = this.state;
        if (isFetching) return <Loading/>;
        return (
            <div className="card">
                <div className="card-header">
                    <strong>Score Employee</strong>
                </div>
                <SweetAlert
                    show={this.state.showSuccess}
                    title="Saved !"
                    type = "success"
                    onConfirm = { () => {this.setState({showSuccess: false})} }
                />
            <div className="card-block">
                { categories.map( (item,key) => {
                    const {category,subCategory} = item;
                    return (
                        <div key={key}>
                            <h4>{category.name + ' (' + category.weightage + '%)'}</h4>
                            <hr/>
                            <form className="form-horizontal">
                            { subCategory.map( (val,ind ) => {
                                return (
                                    <div key={ind} className={ classnames("form-group row offset-md-1", {'has-danger':error[val._id] } ) }>
                                        <label className="col-md-3 form-control-label">{val.name + ' ( Max Score: ' + val.score + ')'}</label>
                                        <div className="col-md-5">
                                            <input type="Number" className="form-control" value={score[val._id]}
                                                   onChange = { (e) => this.onChange(e,val._id)} placeholder="Score.."/>
                                            {error[val._id] && <span className="help-block text-danger">{error[val._id]}</span>}

                                        </div>
                                    </div>
                                )
                            } ) }
                            </form>
                        </div>
                    )
                } ) }
            </div>
                <div className="card-footer">
                    <button type="submit" onClick={this.onSubmit.bind(this)} disabled={isLoading} className="btn btn-primary offset-md-3">
                        {isLoading ? 'Submitting..' : 'Submit'  }</button>
                    {' '}
                    <button onClick={this.onReset.bind(this)} className="btn btn-secondary">Reset</button>
                </div>
            </div>
        )
    }

    onChange(e,categoryId) {
        let score = this.state.score;
        score[categoryId] = parseInt(e.target.value,10);
        this.setState({ score: score });
    }

    onReset(e) {
        e.preventDefault();
        let score = this.state.score;
        for (let [key] of Object.entries(score) ) {
            score[key] = ''
        }
        this.setState({ score: score });
    }

    onSubmit(e) {
        e.preventDefault();
        if(!this.formValidate()) return;
        const {selectedId, selectedWeek, selectedYear} = this.props;
        const {score} = this.state;
        this.setState({ isLoading: true });
        axios.post('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/perfmanage/assignscore', {
            selectedId, selectedYear, selectedWeek, score
        } ).then( res => {
            this.setState({ isLoading: false, showSuccess: true });
        } )
    }

    fetchData(selectedId,selectedWeek,selectedYear) {
        this.setState({isFetching: true});
        axios.get('http://' + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/admin/category/show').then( res => {
            let {score,maxScore} = this.state;
            for (let i = 0;i<res.data.length;i++) {
                for (let j = 0;j<res.data[i].subCategory.length;j++) {
                    score[res.data[i].subCategory[j]._id] = '';
                    maxScore[res.data[i].subCategory[j]._id] = res.data[i].subCategory[j].score;
                }
            }
            this.setState({categories: res.data, score: score,maxScore:maxScore});
            axios.get('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/perfmanage/getscore',{
                params: {selectedId,selectedYear,selectedWeek}
            }).then(res => {
                if (res.data) {
                    let score = {};
                    for(let i = 0;i<res.data.perf.length;i++) {
                        score[res.data.perf[i].categoryId] = res.data.perf[i].score;
                    }
                    this.setState({ score: score });
                }
                this.setState({isFetching: false});
            })
        })
    }

    formValidate() {
        const {score,maxScore} = this.state;
        let error = {}; let isValid = true;
        for ( let [key,val] of Object.entries(score) ) {
            if (val==='') {
                error[key] = 'This field is required';
                isValid = false
            }
            else if (parseInt(val,10) > maxScore[key]) {
                error[key] = 'Score should be less than Max Score';
                isValid= false
            }
        }
        this.setState({ error: error });
        return isValid;
    }

}

export default ScoreForm;