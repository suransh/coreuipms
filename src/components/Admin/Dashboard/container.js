/**
 * Created by prabal on 14/7/17.
 */

import React from 'react';
import Categories from './categories';

class Dashboard extends React.Component {


    render() {
        return (
        <div className="card">
            <div className="card-header">
                <strong><h5> Categories </h5></strong>
            </div>
                <div className="card-block">
                <div className="col-md-12">
                <Categories/>
                </div>
            </div>
        </div>
        )
    }

}

export default Dashboard;