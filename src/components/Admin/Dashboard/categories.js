/**
 * Created by prabal on 14/7/17.
 */

import React from 'react';
import Collapsible from 'react-collapsible';
import './Accordion.css';
import Loading from 'react-loading-animation';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';
import config from '../../../config';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import validate from '../../../utils/ValidatingForms';
import classnames from 'classname';

class Categories extends React.Component {

    constructor() {
        super();
        this.state = {
            selectedId: null,
            info: false,
            name: '',
            weightage: '',
            isFetching: false,
            showAlert: false,
            showSuccess: false,
            toBeDeleted: null,
            modalHeader: null,
            toBeEdited: null,
            error: {},
            open: [],
            data: [],
            score: '',
            onSave: () => {},
            onDelete: () => {}
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    toggleInfo() {
        this.setState({
            info: !this.state.info,
            name: '',
            weightage: '',
            score: ''
        });
    }

    onChange(e) {
        if (e.target.name==='weightage' || e.target.name==='score') {
            this.setState({
                [e.target.name] : e.target.value==='' ? '' : parseInt(e.target.value,10)
            })
        }
        else {
            this.setState({
                [e.target.name]: [e.target.value]
            })
        }
    }

    onAdd(e) {
        e.preventDefault();
        const {isValid,error} = validate({
            name: this.state.name,
            weightage: this.state.weightage,
            score: this.state.score
        });
        if (!isValid) {
            this.setState({
                error: error
            });
            return;
        }
        let req = {
            id: this.state.selectedId,
            subCategory: {
                name: this.state.name,
                weightage: parseInt(this.state.weightage,10),
                score: parseInt(this.state.score,10)
            }
        };
        this.setState({ info: false, isFetching: true});
        axios.post('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/category/addSub', req).then( res => {
            this.setState({selectedId: null });
            this.fetchData();
        } )
    }

    delCategory() {
        this.setState({ isFetching: true });
        axios.delete('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/category/delCategory',
            { data: { id: this.state.toBeDeleted } } ).then( req => {
                this.setState({toBeDeleted: null,isFetching: false });
                this.setState({ showAlert: false });
                this.fetchData();
        } )
    }

    editCategory() {
        const {isValid,error} = validate({
            name: this.state.name,
            weightage: this.state.weightage
        });
        if (!isValid) {
            this.setState({
                error: error
            });
            return;
        }
        this.setState({ isFetching: true, info: false });
        axios.put('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/category/editCategory', {
            id: this.state.selectedId,
            category: {
                name: this.state.name,
                weightage: parseInt(this.state.weightage,10)
            }
        }  ).then( req => {
            this.fetchData();
        } )
    }

    delSubCategory() {
        this.setState({ isFetching: true });
        axios.put('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/category/delSubCategory',
            { selectedId: this.state.selectedId, toBeDeleted: this.state.toBeDeleted } ).then( req => {
                this.setState({ isFetching: false, toBeDeleted: null, selectedId: null }  );
                this.setState( {showAlert: false} );
                this.fetchData();
        } )
    }

    editSubCategory() {
        const {isValid,error} = validate({
            name: this.state.name,
            weightage: this.state.weightage,
            score: this.state.score
        });
        if (!isValid) {
            this.setState({
                error: error
            });
            return;
        }
        this.setState({ isFetching: true, info: false });
        let req = {
            name: this.state.name,
            weightage: parseInt(this.state.weightage,10),
            score: parseInt(this.state.score,10)
        };
        axios.put('http://' + config.APP_SERVER_IP + ':' + config.APP_SERVER_PORT + '/api/admin/category/editSubCategory',
            { selectedId: this.state.selectedId, toBeEdited: this.state.toBeEdited, newCategory: req } ).then( req => {
                this.setState( { isFetching: false, toBeEdited: null, selectedId: null } );
                this.fetchData();
        } )
    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid,error} = validate({
            name: this.state.name,
            weightage: this.state.weightage
        });
        if (!isValid) {
            this.setState({
                error: error
            });
            return;
        }
        const {name,weightage} = this.state;
        this.setState({ isFetching: true });
        let newCategory = { name, weightage: parseInt(weightage,10) };
        axios.post('http://' + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/admin/category/add',{ newCategory: newCategory }).then( res => {
            this.setState({ info: false });
            this.fetchData();
        } )
    }

    render() {
        const {isFetching,open,data} = this.state;
        if (isFetching) return <Loading/>;
        return (
                <div>
                    <button className="btn btn-primary" onClick={() =>
                    {this.setState({ info: true, onSave: this.onSubmit, modalHeader: "Add Category", name:'', weightage:'' })} }>Add Category</button>{' '}
                    <button className="btn btn-secondary" onClick={this.expandAll.bind(this)} >Expand All</button>
                    {this.modal()}
                    {this.sweetAlert()}
                    <br/> <br/>
                    <div/>
                    { data.map( (item,key) => {
                        const {category,subCategory} = item;
                        return (
                            <Collapsible key = {key} open = {open.indexOf(key)!==-1} trigger = { <h6> {category.name} ( {category.weightage}% ) </h6> } >
                                <div>
                                    <div className="float-right" >
                                        <button onClick={()=>
                                        { this.setState({ selectedId: item._id, modalHeader:"Add Sub-Category" ,onSave: this.onAdd ,
                                            info: true, open: [key], name: '', weightage: '', score: '' });  } }  className=" btn btn-outline-info fa fa-plus"/>{' '}
                                        <button className="btn btn-outline-secondary fa fa-edit" onClick={ () => {
                                            this.setState( { modalHeader: "Edit Category", name: item.category.name, weightage: item.category.weightage,
                                                onSave: this.editCategory , info: true, selectedId: item._id, open: [key]} );  } }
                                        />{' '}
                                        <button className="btn btn-outline-danger fa fa-remove"
                                                onClick={ () => this.setState( {showAlert: true, toBeDeleted: item._id, onDelete: this.delCategory, open: [key] } )}/>{' '}
                                    </div>
                                    <br/> <br/>
                                    { subCategory.length === 0 ? null :
                                        <div className="card-block col-md-7 offset-md-2">
                                            <table className="table table-bordered">
                                                <thead className="card-header ">
                                                <tr>
                                                    <th>Category</th>
                                                    <th>Score</th>
                                                    <th>Weightage</th>
                                                    <th/>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {subCategory.map((val, ind) => {
                                                    return (
                                                        <tr key={ind}>
                                                            <td> {val.name} </td>
                                                            <td>{val.score}</td>
                                                            <td> {val.weightage + '%'} </td>
                                                            <td><span onClick={() => {
                                                                this.setState({
                                                                    modalHeader: "Edit Sub-Category",
                                                                    name: val.name,
                                                                    weightage: val.weightage,
                                                                    score: val.score,
                                                                    info: true,
                                                                    selectedId: item._id,
                                                                    toBeEdited: val._id,
                                                                    onSave: this.editSubCategory,
                                                                    open: [key]
                                                                })
                                                            }}
                                                                      className="fa fa-edit"/>{' '}
                                                                <span onClick={() => {
                                                                    this.setState({
                                                                        showAlert: true,
                                                                        selectedId: item._id,
                                                                        toBeDeleted: val._id,
                                                                        onDelete: this.delSubCategory,
                                                                        open: [key]
                                                                    })
                                                                }}
                                                                      className="fa fa-remove"/></td>
                                                        </tr>
                                                    )
                                                })}
                                                </tbody>
                                            </table>
                                        </div>
                                    }
                                </div>
                            </Collapsible>
                        )
                    } ) }
                </div>

        )
    }

    modal() {
        const type = this.state.modalHeader;
        const {error} = this.state;
        return(
        <Modal isOpen={this.state.info} toggle={this.toggleInfo.bind(this)} className={'modal-info'} keyboard = {true}  >
            <ModalHeader toggle={this.toggleInfo.bind(this)}>{type}</ModalHeader>
            <ModalBody>
                <div className="card-block">
                    <form >
                        <div className={ classnames("form-group offset-md-2", {'has-danger':error.name } ) }>
                            <label className="col-md-2">Name</label>
                            <div className="col-md-10">
                                <input className="form-control" type="text"
                                       value={this.state.name} onChange={this.onChange.bind(this)} name="name" placeholder="Category..." />
                            </div>
                            {error.name && <span className="help-block text-danger col-md-2">{error.name}</span>}
                        </div>
                        <div className={ classnames("form-group offset-md-2", {'has-danger':error.weightage } ) }>
                            <label className="col-md-2 ">Weightage(%)</label>
                            <div className="col-md-10">
                                <input className="form-control" type="Number"
                                       value={this.state.weightage} onChange={this.onChange.bind(this)} name="weightage" placeholder="Weightage..." />
                            </div>
                            {error.weightage && <span className="help-block text-danger col-md-2">{error.weightage}</span>}
                        </div>
                        {type !=='Add Category' && type !== 'Edit Category' ?
                        <div className={ classnames("form-group offset-md-2", {'has-danger':error.score } ) }>
                            <label className="col-md-2 ">Score</label>
                            <div className="col-md-10">
                                <input className="form-control" type="Number"
                                       value={this.state.score} onChange={this.onChange.bind(this)} name="score" placeholder="Score..." />
                            </div>
                            {error.score && <span className="help-block text-danger col-md-2">{error.score}</span>}
                        </div> : null }
                    </form>
                </div>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" type = "submit" onClick={ this.state.onSave.bind(this) }>Save</Button>{' '}
                <Button color="secondary" onClick={() => { this.setState({ info: false, selectedId: null, toBeDeleted: null,
                toBeEdited:null }) }}>Cancel</Button>
            </ModalFooter>
        </Modal>
        )
    }

    sweetAlert() {
        return (
            <SweetAlert
                show={this.state.showAlert}
                title="Are you sure ?"
                type="warning"
                showCancelButton= {true}
                showLoaderOnConfirm = {true}
                confirmButtonColor = {"#DD6B55"}
                confirmButtonText = {"Yes, delete it!"}
                onConfirm = { this.state.onDelete.bind(this) }
                onCancel = {() => {this.setState({ showAlert: false, toBeDeleted: null, selectedId: null })}}
            />
        )
    }

    fetchData() {
        this.setState({isFetching: true});
        axios.get('http://' + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/admin/category/show').then( res => {
            this.setState({
                data: res.data,
                isFetching: false
            })
        } );
    }

    expandAll() {
        const {open} = this.state;
        if (open.length !==0 ) this.setState({ open: [] });
        else {
            let openNew = [];
            for (let i = 0; i < this.state.data.length; i++) openNew.push(i);
            this.setState({open: openNew});
        }
    }

}

export default Categories;
