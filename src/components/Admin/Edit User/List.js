/**
 * Created by prabal on 3/7/17.
 */

import React from 'react';
import Pagination from "./Pagination";
import axios from 'axios';
import Loading from 'react-loading-animation';
import UserRow from './UserRow';
import json from './listOrder.json';
import queryString from 'query-string';
import {withRouter} from 'react-router-dom';
import config from '../../../config';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';

class List extends React.Component {

    constructor() {
        super();
        this.state = {
            activePage:1,
            totalItemsCount: 15,
            isFetching: true,
            data: null,
            sortBy: {
                col: null,
                type: null
            },
            search: null,
            showAlert: false,
            showSuccess: false,
            toBeDeleted: null
        }
    }

    componentDidMount() {
        let query = queryString.parse(this.props.location.search);
        let pageNumber = this.props.match.params.page;
        this.fetchData(pageNumber,query);
    }

    componentWillReceiveProps(nextProps) {
        let query = queryString.parse(nextProps.location.search);
        let pageNumber = nextProps.match.params.page;
        this.fetchData(pageNumber,query);
    }

    render() {
        const {isFetching, data, totalItemsCount,sortBy} = this.state;
        const {headings,name} = json;
        if (isFetching) return <Loading/>;
        return (
                <div className="card">
                    <div className="card-header">
                        <strong><h5>Users</h5></strong>
                    </div>
                    <div className="card-block" style={{"overflowX" : "auto"}}>
                            <form action="" className="form-horizontal">
                                <div className="form-group row">
                                    <div className="col-md-4">
                                        <div className="input-group">
                                            <input type="text" name="search" className="form-control" onChange={this.onChange.bind(this)} value={this.state.search} placeholder="Search"/>
                                            <span className="input-group-btn">
                          <button type="button" onClick={this.onSearch.bind(this)} className="btn btn-primary"><i className="fa fa-search"/> Search</button>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <SweetAlert
                            show={this.state.showAlert}
                            title="Are you sure ?"
                            type="warning"
                            text="You will permanently delete the user"
                            showCancelButton= {true}
                            showLoaderOnConfirm = {true}
                            confirmButtonColor = {"#DD6B55"}
                            confirmButtonText = {"Yes, delete it!"}
                            onConfirm = { () => {this.onDelete(this.state.toBeDeleted)} }
                            onCancel = {() => {this.setState({ showAlert: false, toBeDeleted: null })}}
                        />
                        <SweetAlert
                            show={this.state.showSuccess}
                            title="Deleted!"
                            type="success"
                            text="User has been deleted"
                            onConfirm = { () => {this.setState( {showSuccess:false} )} }
                        />
                        <table className="table table-bordered">
                            <thead className="card-header text-center">
                            <tr>
                                { headings.map( (val,ind) => {
                                    return (<th key={ind} >
                                        <a onClick={ this.sortByColumn.bind(this,ind) }>{val} </a>
                                        <span>
                                            { sortBy.col===name[ind] ? (sortBy.type==='INC' ? <span className="fa fa-sort-asc"/>
                                                : <span className="fa fa-sort-desc"/>) : null }
                                        </span>
                                    </th>)
                                } ) }
                                <th/>
                            </tr>
                            </thead>
                            <tbody>
                                { data.map( (item,ind)  => {
                                    return <UserRow onDelete = { (id) => { this.setState({showAlert: true, toBeDeleted: id} ); } }
                                                    onEdit = { (ind) => this.onEdit(ind) } key={ind} item={item} index={ind}/>
                                }) }
                            </tbody>
                        </table>
                        <Pagination  totalItemsCount={totalItemsCount} activePage={parseInt(this.props.match.params.page,10)} />
                    </div>
                </div>
        )
    }

    fetchData(pageNumber,query) {
        this.setState({ isFetching: true });
        axios.get('http://' + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/admin/list', {params: {page: pageNumber,
            col: query.col,
            type: query.type,
            search: query.search
        }
    }).then( (res) => {
            //Logic : Redirecting to last possible page
            let page = parseInt(pageNumber,10), cnt = res.data.count;
            let maxPage = Math.ceil(cnt/2);
            if ( maxPage!==0 && maxPage < page) {
                let redirect = '/admin/list/' + maxPage;
                if (query.search) redirect += '?' + queryString.stringify(query);
                this.props.history.push(redirect);
            }
            else {
                this.setState({
                    isFetching: false,
                    data: res.data.result,
                    totalItemsCount: res.data.count,
                    activePage: page,
                    search: (!query.search ? "" : query.search),
                    sortBy: {col: query.col, type: query.type}
                });
            }
        });
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSearch(e) {
        e.preventDefault();
        const {search} = this.state;
        this.props.history.push('/admin/list/1?search='+ search);
    }

    onDelete(id) {
        axios.delete('http://'+ config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/admin/remove', {
            data: { id: id }
        }).then( (res) => {
            this.setState({ showAlert:false, showSuccess: true, toBeDeleted: null });
            let pageNumber = this.props.match.params.page;
            let query = queryString.parse(this.props.location.search);
            this.fetchData(pageNumber,query);
        } )
    }

    onEdit(index) {
        const {data} = this.state;
        const query = queryString.stringify(data[index]);
        this.props.history.push('/admin/edit/?' + query);
    }


    sortByColumn(ind) {
        const {name} = json;
        let sortBy = Object.assign({},this.state.sortBy);
        if ( !(sortBy.col===name[ind]) ) { sortBy.col = name[ind]; sortBy.type = "INC" }
        else if (sortBy.type==="DEC") { sortBy.col = null; sortBy.type = null }
        else {
            sortBy.col = name[ind];
            if (sortBy.type === null) sortBy.type = "INC";
            else sortBy.type = "DEC";
        }
        const query = queryString.stringify(sortBy);
        this.props.history.push('/admin/list/1?' + query + "&search=" + this.state.search);
    }
}

export default withRouter(List);