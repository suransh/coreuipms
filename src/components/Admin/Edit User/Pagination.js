/**
 * Created by prabal on 6/7/17.
 */

import React, { Component } from "react";
import {withRouter} from 'react-router-dom';
import Pagination from "react-js-pagination";

class PageNumber extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activePage: 1
        };
    }

    handlePageChange(pageNumber) {
        this.setState({activePage: pageNumber});
        this.props.history.push('/admin/list/' + pageNumber + this.props.location.search);
    }

    render() {
        return (
            <div>
                <Pagination
                    activePage={this.props.activePage}
                    itemsCountPerPage={5}
                    totalItemsCount={this.props.totalItemsCount}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange.bind(this)}
                />
            </div>
        );
    }
}

export default withRouter(PageNumber);
