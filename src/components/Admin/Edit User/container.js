/**
 * Created by prabal on 3/7/17.
 */

import React from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
import UserList from './List';
import addEmp from './addEmp';
import EditEmp from './EditEmp';
import PerfManage from '../Performace Management/container';

class container extends React.Component {

    render() {
        return (
                <Switch >
                    <Route exact path="/admin/list/:page" component={UserList}/>
                    <Route exact path="/admin/add" component={addEmp} />
                    <Route path="/admin/perfmanage" component={PerfManage}/>
                    <Route path="/admin/edit" component={EditEmp} />
                    <Redirect from='/admin/list' to="/admin/list/1" />
                </Switch>
        )
    }

}

export default container;