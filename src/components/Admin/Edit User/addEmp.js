/**
 * Created by prabal on 3/7/17.
 */

import React from 'react';
import json from './addEmp.json';
import validate from '../../../utils/ValidatingForms';
import classnames from 'classname';
import axios from 'axios';
import config from '../../../config';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import {withRouter} from 'react-router-dom';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import {connect} from 'react-redux';

class addEmp extends React.Component {

    constructor() {
        super();
        this.state = {
            id: '',
            firstName: '',
            lastName: '',
            email: '',
            error: {},
            role: '',
            active: 'true',
            isLoading: false,
            showSuccess: false
        }
    }

    onChange(e) {
        if (e.target.name==='active') {
            this.setState({active: e.target.checked})
        }
        else {
            this.setState({
                [e.target.name]: e.target.value
            });
        }
    }

    onSelect(e) {
        this.setState({ role: e ? e.label : '' });
    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid,error} = validate(this.state);
        if (!isValid) {
            this.setState({
                error: error
            })
        }
        else {
            this.setState({isLoading:true});
            const {id,firstName,lastName,email,role,active} = this.state;
            let user = {
                id,firstName,lastName,email,role,active
            };
            axios.post('http://' + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/admin/add',user).then( (res) => {
                this.setState({
                    isLoading: false,
                    error: res.data.error,
                    success: res.data.success
                });
                if (res.data.success.form) this.setState({ showSuccess: true });
            } )
        }
    }

    render() {
        const {error,isLoading} = this.state;
        const {role} = this.props.user;
        return (
            <div className="card" >
                <div className="card-header">
                    <strong><h5> Add User </h5></strong>
                </div>
                <SweetAlert
                    show={this.state.showSuccess}
                    title="Added !"
                    type="success"
                    text="An email for setting your password will be send shortly"
                    onConfirm = { () => {this.setState({showSuccess: false})} }
                />
                <div className="card-block">
                    <form className="form-horizontal">
                        { json.map( (item,key) => {
                            const {attr} = item;
                            return (
                                <div key={key}>
                                    { attr.type==='checkbox' ?
                                        (
                                            <div className="form-group row">
                                                <label className="col-md-2 offset-md-2 form-control-label">{item.label}</label>
                                                <div className="col-md-4">
                                                    <label className="switch switch-default switch-pill switch-primary-outline-alt">
                                                    <input type="checkbox" name = {attr.name} className="switch-input" onChange={this.onChange.bind(this)} checked = {this.state[attr.name]}/>
                                                    <span className="switch-label"/>
                                                    <span className="switch-handle"/>
                                                </label>
                                                </div>
                                            </div>) :
                                        (<div className={ classnames("form-group row", {'has-danger':error[attr.name] } ) }>
                                        <label className="col-md-2 offset-md-2 form-control-label">{item.label}</label>
                                        <div className="col-md-4">
                                        { item.tag !== 'select' ?
                                            (<item.tag className = {attr.className}  type={attr.type} name={attr.name}
                                                       onChange={this.onChange.bind(this)} placeholder={attr.placeholder}
                                                       value={attr.value}/> ) :
                                            <Select
                                                name="role"
                                                value={this.state.role}
                                                options={role===config.superAdmin ? item.options : item.options.slice(0,-1)}
                                                onChange={this.onSelect.bind(this)}
                                            />
                                        }
                                    {error[attr.name] && <span className="help-block text-danger">{error[attr.name]}</span>}
                                    </div>
                                </div>
                            )
                        }
                                </div>
                            ) } )
                        }
                    </form>
                </div>
                    <div className="card-footer">
                            <button className="btn btn-primary offset-md-4" onClick={this.onSubmit.bind(this)} disabled={isLoading}>
                                { isLoading ? "Submitting..." : "Submit Form" }</button>
                            <span> </span>
                            <button className="btn btn-secondary" onClick={() => this.props.history.push('/admin/list')} >Cancel</button>
                        </div>
                </div>
        )
    }
}

export default withRouter(connect((state) => ( {user: state.auth.user} ),null)(addEmp));
