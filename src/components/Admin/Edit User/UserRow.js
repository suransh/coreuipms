/**
 * Created by prabal on 4/7/17.
 */

import React from 'react';
import json from './listOrder.json';

class UserRow extends React.Component {

    render() {
        const {name} = json;
        const {index,item} = this.props;
        return (
            <tr key={index}>
                { name.map((val, ind) => {
                    if (val === 'active') {
                        if (item[val]) {
                            return (
                                <td key={ind}><span className="badge badge-success">True</span></td>
                            )
                        }
                        else {
                            return (
                                <td key={ind}><span className="badge badge-danger">False</span></td>
                            )
                        }
                    }
                    return (<td key={ind}>{item[val]}</td>)
                }) }
                <td>
                    <span className="fa fa-edit" onClick={() => this.onClickEdit(index)} style={{"cursor": "pointer"}}/>
                    <span>  </span>
                    <span onClick={() => this.onClickDelete(item._id)} className="fa fa-remove" style={{"cursor": "pointer"}}/>
                </td>
            </tr>
        )
    }
    onClickDelete (uid) {
        this.props.onDelete(uid);
    }
    onClickEdit(ind) {
        this.props.onEdit(ind);
    }
}
export default UserRow;


