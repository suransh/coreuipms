/**
 * Created by prabal on 5/7/17.
 */

import React from 'react';
import json from './addEmp.json';
import classnames from 'classname'
import validate from '../../../utils/ValidatingForms';
import axios from 'axios';
import queryString from 'query-string';
import config from '../../../config';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import {connect} from 'react-redux';

class EditEmp extends React.Component {

    constructor() {
        super();
        this.state = {
            error: {},
            isLoading: false,
            showSuccess: false
        }
    }

    onSelect(e) {
        this.setState({ role: e ? e.label : '' });
    }

    onChange(e) {
        if (e.target.type==='checkbox') {
            this.setState({active: e.target.checked.toString()});
        }
        else {
            this.setState({
                [e.target.name]: e.target.value
            });
        }
    }

    componentWillMount() {
        const state = queryString.parse(queryString.extract(this.props.location.search));
        for ( let [key,val] of Object.entries(state) ) this.setState({ [key]: val});
        this.setState({uid : state._id});
    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid,error} = validate(this.state);
        if (!isValid) {
            this.setState({
                error: error
            })
        }
        else {
            const {id,firstName,lastName,email,role,active,uid} = this.state;
            let user = {
                id,firstName,lastName,email,role,active
            };
            this.setState({isLoading:true});
            axios.post("http://" + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + "/api/admin/edit",{
                uid,
                user
            }).then( (res) => {
                this.setState({
                    isLoading: false,
                    error: res.data.error,
                    success: res.data.success
                });
                if (res.data.success.form) this.setState({ showSuccess: true });
            } )

        }
    }

    render() {
        const {error,isLoading} = this.state;
        const {role} = this.props.user;
        return (
            <div className="card" >
                <div className="card-header">
                    <strong><h5> Edit User </h5></strong>
                </div>
                <SweetAlert
                    show={this.state.showSuccess}
                    title="Edited !"
                    type="success"
                    text="Changes Saved Succesfully "
                    onConfirm = { () => {this.setState({showSuccess: false})} }
                />
                <div className="card-block">
                    <form className="form-horizontal">
                        { json.map( (item,key) => {
                            const {attr} = item;
                            return (
                                <div key={key}>
                                    { attr.type==='checkbox' ?
                                        (
                                            <div className="form-group row">
                                                <label className="col-md-2 offset-md-2 form-control-label">{item.label}</label>
                                                <div className="col-md-4">
                                                    <label className="switch switch-default switch-pill switch-primary-outline-alt">
                                                        <input type="checkbox" name = {attr.name} className="switch-input"
                                                               onChange={this.onChange.bind(this)} checked = {this.state[attr.name]==='true'}/>
                                                        <span className="switch-label"/>
                                                        <span className="switch-handle"/>
                                                    </label>
                                                </div>
                                            </div>) :
                                        (<div className={ classnames("form-group row", {'has-danger':error[attr.name] } ) }>
                                                <label className="col-md-2 offset-md-2 form-control-label">{item.label}</label>
                                                <div className="col-md-4">
                                                    { item.tag !== 'select' ?
                                                        (<item.tag className = {attr.className}  type={attr.type} name={attr.name}
                                                                   onChange={this.onChange.bind(this)} placeholder={attr.placeholder}
                                                                   value={this.state[attr.name]}/> ) :
                                                        <Select
                                                            name="role"
                                                            value={this.state[attr.name]}
                                                            options={ role===config.superAdmin ? item.options : item.options.slice(0,-1)}
                                                            onChange={this.onSelect.bind(this)}
                                                        />
                                                    }
                                                    {error[attr.name] && <span className="help-block text-danger">{error[attr.name]}</span>}
                                                </div>
                                            </div>
                                        )
                                    }
                                </div>
                            ) } )
                        }
                    </form>
                </div>
                <div className="card-footer">
                    <button className="btn btn-primary offset-md-4" onClick={this.onSubmit.bind(this)} disabled={isLoading}>
                        { isLoading ? "Submitting..." : "Submit Form" }</button>
                    <span> </span>
                    <button className="btn btn-secondary" onClick={() => this.props.history.push('/admin/list')} >Cancel</button>
                </div>
            </div>
        )
    }

}

export default connect( (state) => ({ user: state.auth.user }),null )(EditEmp);