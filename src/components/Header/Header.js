import React, { Component } from 'react';
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';
import {logout} from '../../actions/authActions';
import {withRouter,Link} from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

    onLogout(e) {
        e.preventDefault();
        this.props.logout();
        this.props.history.push('/login');
    }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  render() {
    return (
      <header className="app-header navbar">
        <button className="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button" onClick={this.mobileSidebarToggle}>&#9776;</button>
        <Link className="navbar-brand" to="/"/>
        <ul className="nav navbar-nav d-md-down-none">
          <li className="nav-item">
            <button className="nav-link navbar-toggler sidebar-toggler" type="button" onClick={this.sidebarToggle}>&#9776;</button>
          </li>
          <li className="nav-item px-3">
            <Link className="nav-link" to="/">Dashboard</Link>
          </li>
        </ul>
        <ul className="nav navbar-nav ml-auto">
          <li className="nav-item">
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <button onClick={this.toggle} className="nav-link dropdown-toggle" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
                <span className="d-md-down-none"> <i className="icon-user" /> {this.props.firstName}</span>
              </button>

              <DropdownMenu className="dropdown-menu-right">

                <DropdownItem header className="text-center"><strong>Account</strong></DropdownItem>
                <Link to="/profile/up" > <DropdownItem><i className="fa fa-tasks"/> Update Password</DropdownItem></Link>

                <DropdownItem header className="text-center"><strong>Settings</strong></DropdownItem>

                <Link to="/profile" ><DropdownItem> <i className="fa fa-user"/> Profile</DropdownItem></Link>
              <DropdownItem onClick={this.onLogout.bind(this)} ><a><i className="fa fa-lock"/> Logout</a></DropdownItem>

              </DropdownMenu>
            </Dropdown>
          </li>
        </ul>
      </header>
    )
  }
}

export default withRouter(connect( (state) => ( {firstName: state.auth.user.firstName } ),{logout})(Header));
