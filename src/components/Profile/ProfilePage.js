/**
 * Created by prabal on 23/6/17.
 */

import React from 'react';
import UserInfo from './UserInfo';
import {withRouter,Switch,Route} from 'react-router-dom';
import EditProfile from './EditProfile';
import UpdatePassword from './Update Pass';


class ProfilePage extends React.Component {

    cardHeader(path) {
        if (path==='/profile/up') return "Update Password";
        else if (path==='/profile/edit') return "Edit Profile";
        else return "User Profile";
    }

    render() {
        const {pathname} = this.props.location;
        return (
            <div className="animated fadeIn">
                <div className="card">
                    <div className="card-header">
                        <strong><h5>
                            { this.cardHeader(pathname) }
                        </h5></strong>
                    </div>
                    <div className="card-block">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4 text-center">
                                    <img alt="User Pic" src="../img/profile-icon.png" width={"100%"}/>
                                </div>
                                        <Switch>
                                            <Route exact path="/profile" component={UserInfo} />
                                            <Route exact path="/profile/edit" component={EditProfile} />
                                            <Route exact path="/profile/up" component={UpdatePassword} />
                                        </Switch>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ProfilePage);