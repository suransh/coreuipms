/**
 * Created by prabal on 26/6/17.
 */

import React from 'react';
import json from './EditProfile.json';
import validate from '../../utils/ValidatingForms'
import classnames from 'classname';
import {connect} from 'react-redux';
import axios from 'axios';
import {editProfile} from '../../actions/authActions';
import config from '../../config';
import {withRouter} from 'react-router-dom';

class EditProfile extends React.Component {

    constructor() {
        super();
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            error: {},
            success: {},
            isLoading: false
        }

    }

    onChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        })

    }

    componentWillMount() {
        const {user} = this.props;
        json.map ( (item,key) => {
            const {attr} = item;
            this.setState({ [attr.name] : user[attr.name] })
        } )
    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid,error} = validate(this.state);
        if (!isValid) {
            this.setState({
                error: error
            })
        }
        else {
            this.setState({error: {}, isLoading:true});
            let apiUrl = "http://" + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + "/api/profile/edit";
            let req = Object.assign({},{ id: this.props.user.uid }, this.state);
            axios.post(apiUrl, req).then( (res) => {
                    this.setState({
                       error: res.data.error,
                        success: res.data.success,
                        isLoading: false
                    });
                    this.props.editProfile(res.data.user);
            }).catch((err) => {
                console.log(err);
                this.setState({
                    error: { form: "Something went wrong !! Please Try Again"},
                    isLoading: false
                })
            })
        }
    }

    render() {
        const {error,success,isLoading} = this.state;
        return (
            <div className="col-md-6">
                <div style={{"overflowX": "auto" }}>
            <div className="container">
                        { success.form && <div className="alert alert-success text-center">{success.form}</div> }
                        { error.form && <div className="alert alert-danger text-center">{error.form}</div> }
                        <div className="card-block">
                            <form className="form-horizontal">

                                { json.map( (item,key) => {
                                    const {attr} = item;
                                  return (
                                      <div key={key} className={ classnames("form-group row", {'has-danger':error[attr.name] } ) }>
                                          <label className="col-md-3 form-control-label">{item.label}</label>
                                          <div className="col-md-8">
                                              <item.tag className = {attr.className} type={attr.type} name = {attr.name}
                                                        onChange={this.onChange.bind(this)} placeholder = {attr.placeholder} value = {this.state[attr.name]} />
                                              {error[attr.name] && <span className="help-block text-danger">{error[attr.name]}</span>}
                                          </div>
                                      </div>
                                  )
                                }) }
                                <br/>
                                <div className="form-group offset-md-1">
                                        <input type="submit" className="btn btn-primary" disabled={isLoading}
                                               value={isLoading ? "Saving" : "Save"} onClick={this.onSubmit.bind(this)}  />
                                    <span> </span>
                                    <input type="button" className="btn btn-secondary" value="Cancel"
                                           onClick={() => {this.props.history.push('/profile')}}/>
                                </div>
                            </form>
                                </div>
            </div>
                </div>
            </div>
        )
    }

}

export default withRouter ( connect( (state) => ( {user: state.auth.user} ),{ editProfile })(EditProfile) );
