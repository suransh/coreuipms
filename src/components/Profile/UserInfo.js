/**
 * Created by prabal on 26/6/17.
 */

import React from 'react';
import { connect } from 'react-redux';
import json from './profile.json';
import {Link} from 'react-router-dom';

class UserInfo extends React.Component {

    constructor() {
        super();
        this.state = {}
    }

    componentWillMount() {
        this.setState(this.props.user);
    }

    render() {

        return (
            <div className="col-md-6">
                <br/>
                <div style={{"overflowX": "auto" }}>
            <table className="table text-center" >
                <tbody>
                {json.map( (item,key) => {
                    return ( <tr key={key}>
                        <td> <strong>{item.label}</strong></td>
                        <td className="text-left">{this.state[item.name]}</td>
                    </tr>
                    )
                } )}
                </tbody>
            </table>
                </div>
                <Link to="/profile/edit" className="btn btn-primary"><i className="fa fa-edit" /> Edit Profile </Link>
            </div>
        )
    }
}

export default connect( (state) => state.auth , null )(UserInfo);
