import React, { Component } from 'react';
import {connect} from 'react-redux';
import validateInput from '../../utils/ValidatingForms';
import classnames from 'classname';
import axios from 'axios';
import config from '../../config';

class UpdatePass extends Component {

    constructor() {
        super();
        this.state = {
            oldPassword: '',
            newPassword: '',
            confirmPassword: '',
            error: {},
            success: {},
            isLoading: false
        }
    }

    onChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        })

    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid, error} = validateInput(this.state);
        if (!isValid) {
            this.setState({
                error: error
            });
        }
        else {
            const {user} = this.props.auth;
            let req = Object.assign({},this.state,{userId: user.uid});
            this.setState({error: {}, isLoading: true});
            axios.post("http://"+ config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + "/api/up",req).then( (res) => {
                this.setState( {success: res.data.success, isLoading: false,error: res.data.error });
            }).catch( (err)=> {
                throw err;
            })
        }
    }


    render() {
      const {success, error,isLoading} = this.state;
    return (
        <div className="col-md-6">

            <div style={{"overflowX": "auto" }}>
                            { success.form && <div className="alert alert-success">{success.form}</div>  }
                            { error.form && <div className="alert alert-danger">{error.form}</div>  }
                    <div className="card-block">
                            <form className="form-horizontal">
                                <div className={ classnames("form-group row", {'has-danger' : error.oldPassword} ) }>
                                    <label htmlFor="nf-password">Old Password</label>
                                    <input type="password" id="nf-password" name="oldPassword" onChange={this.onChange.bind(this)} className="form-control" placeholder="Old Password.."/>
                                    {error.oldPassword && <span className="help-block text-danger">{error.oldPassword}</span>}
                                </div>
                                <div className={ classnames("form-group row", {'has-danger' : error.newPassword} ) }>
                                    <label htmlFor="nf-password">New Password</label>
                                    <input type="password" id="nf-password" name="newPassword" onChange={this.onChange.bind(this)} className="form-control" placeholder="New Password.."/>
                                    {error.newPassword && <span className="help-block text-danger">{error.newPassword}</span>}
                                </div>
                                <div className={ classnames("form-group row", {'has-danger' : error.confirmPassword} ) }>
                                    <label htmlFor="nf-password">Confirm Password</label>
                                    <input type="password" id="nf-password" name="confirmPassword" onChange={this.onChange.bind(this)} className="form-control" placeholder="Confirm Password.."/>
                                    {error.confirmPassword && <span className="help-block text-danger">{error.confirmPassword}</span>}
                                </div>
                                <br/>
                                <div className="form-group row">
                                    <input type="submit" className="btn btn-primary" disabled={isLoading}
                                           value={isLoading ? "Saving" : "Save"} onClick={this.onSubmit.bind(this)}  />
                                </div>
                            </form>
                        </div>
                </div>
            </div>
    );
  }
}

export default connect( (state) => ({ auth: state.auth}), null )(UpdatePass);
