/**
 * Created by prabal on 26/6/17.
 */

import validate from 'validate.js';

let EmptyCheck = ["password","oldPassword","newPassword","firstName","lastName","role","id","name",
    'selectedId','selectedYear','selectedWeek'];

function validateData(data,constraints) {
    if (data.hasOwnProperty("email")) {
        constraints["email"] = {
            presence: {
                attribute: true,
                    message:"^This field is required"
            },
            email: {
                attribute: true,
                    message:"^Invalid email"
            }
        }
    }
    EmptyCheck.map( function (item) {
        if (data.hasOwnProperty(item)) {
            constraints[item] = {
                presence: {
                    attribute: true,
                    message: "^This field is required"
                }
            }
        }
    });

    if (data.hasOwnProperty('confirmPassword')) {
        constraints["confirmPassword"] = {
                presence: {
                    attribute: true,
                    message:"^This field is required"
                },
                equality: {
                    attribute: 'newPassword',
                    message:"^Passwords do not match"
                }
        }
    }

    if (data.hasOwnProperty('weightage')) {
        constraints['weightage'] = {
            presence: {
                attribute: true,
                message:"^This field is required"
            },
            numericality: {
                lessThanOrEqualTo: 100,
                message:"^Weightage should be less than 100%"
            }
        }
    }

    return validate(data,constraints);
}

export default function (data) {
    let constraints = {};
    let error = validateData(data,constraints);
    if (error) {
        for ( let [key,val] of Object.entries(error) ) {
            error[key] = val[0];
        }
        return {
            isValid: false,
            error: error
        }
    }
    else {
        return {
            isValid: true,
            error: {}
        }
    }
}
