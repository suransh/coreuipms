import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Footer from '../../components/Footer/';
import Dashboard from '../../views/Dashboard/'
import AdminDashboard from '../../components/Admin/Dashboard/container';
import ProfilePage from '../../components/Profile/ProfilePage';
import Admin from '../../components/Admin/Edit User/container';
import config from '../../config';

class Full extends Component {

  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <div className="container-fluid" style={{"padding":"1%"}}>
              <Switch>
                <Route exact path="/dashboard" name="Dashboard" render={() => (
                    this.checkForAdmin() ? <AdminDashboard/> : <Dashboard/>
                )}/>
                  <Route path="/profile" component={ProfilePage} />
                  { !this.checkForAdmin() ? <Redirect from="/admin" to="/dashboard" /> : null }
                <Route path="/admin" component={Admin} />
                <Redirect from='/' to="/dashboard" />
              </Switch>
            </div>
          </main>
        </div>
        <Footer />
      </div>
    );
  }

  checkForAdmin() {
      const {role} = this.props.user;
      if (role===config.admin || role===config.superAdmin) return true;
      return false;
  }

}

export default withRouter(connect ((state) => ({user: state.auth.user}),null)(Full));
