import React, { Component } from 'react';
import validateInput from '../../../utils/ValidatingForms';
import classnames from 'classname';
import {withRouter,Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { login } from '../../../actions/authActions';

class Login extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            error: {},
            isLoading: false
        }
    }

    onChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        });

    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid, error} = validateInput(this.state);
        if (!isValid) {
            this.setState({
                error: error
            });
        }
        else {
            this.setState({error: {}, isLoading: true});
            this.props.login(this.state).then(
                (res) => { this.setState( {isLoading:false}); this.props.history.push('/dashboard'); },
                (err) => this.setState({error: err.response.data.error, isLoading: false})
            );
        }
    }

  render() {
      const {error, isLoading} = this.state;
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-6">
              <div className="card-group mb-0">
                <div className="card p-4">
                  <div className="card-block">
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p> <br/>
                      {error.form && <span className=" alert alert-danger d-md-block "> {error.form}</span>}
                      <form>
                      <div className={ classnames("form-group", {'has-danger' : error.email } ) }>
                          <div className="input-group mb-3">
                          <span className="input-group-addon"><i className="icon-user"/></span>
                          <input type="email" name="email" onChange={this.onChange.bind(this)} className="form-control" placeholder="Enter Email.."/>
                          </div>
                          {error.email && <span className="help-block text-danger">{error.email}</span>}
                      </div> <br/>
                      <div className={ classnames("form-group", {'has-danger' : error.password } ) }>
                          <div className="input-group mb-3">
                              <span className="input-group-addon"><i className="icon-lock"/></span>
                              <input type="password" name="password" onChange={this.onChange.bind(this)} className="form-control" placeholder="Enter Password.."/>
                          </div>
                          {error.password && <span className="help-block text-danger">{error.password}</span>}
                      </div> <br/>
                    <div className="row">
                      <div className="col-6">
                        <button type="submit" className="btn btn-primary px-4" onClick={this.onSubmit.bind(this)} disabled={isLoading} >Login</button>
                      </div>
                      <div className="col-6 text-right">
                          <button type="button" className="btn btn-link px-0"> <Link to="/reset"> Forgot password? </Link> </button>
                      </div>
                    </div>
                      </form>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    );
  }
}

export default withRouter(connect( null,{login} )(Login));
