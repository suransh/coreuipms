import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import validateInput from '../../../utils/ValidatingForms';
import classnames from 'classname';
import axios from 'axios';
import config from '../../../config';

class Register extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            error: {},
            success: {},
            isLoading: false
        }
    }

    onChange(e) {
        this.setState({
            [e.target.name] : e.target.value
        })

    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid,error} = validateInput(this.state);
        if (!isValid) {
            this.setState({
                error: error
            });
        }
        else {
            this.setState({error: {}, isLoading: true});
            axios.post("http://" + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + "/api/reset",{email:this.state.email}).then( (res) => {
                this.setState({ isLoading: false,error: res.data.error, success: res.data.success });
            }).catch( (err)=> {
                this.setState( {isLoading:false} );
                throw err;
            })
        }
    }

  render() {
      const {success, error,isLoading} = this.state;
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-6">
              <div className="card mx-4">
                <div className="card-block p-4">
                  <h1>Forgot Your Password ?</h1> <br/>
                  <p className="text-muted">Enter the Email address you use to log in to your account
                  <br/>  We'll send you an email with instructions to choose a new password.</p>
                    { success.form && <div className="alert alert-success">{success.form}</div>  }
                    { error.form && <div className="alert alert-danger">{error.form}</div>  }
                    <br/>
                    <div className={ classnames("form-group", {'has-danger' : error.email } ) }>
                        <div className="input-group mb-3">
                            <span className="input-group-addon">@</span>
                            <input type="email" name="email" onChange={this.onChange.bind(this)} className="form-control" placeholder="Enter Email.."/>
                        </div>
                        {error.email && <span className="help-block text-danger">{error.email}</span>}
                    </div> <br/>
                  <button type="button" disabled={isLoading} onClick={this.onSubmit.bind(this)} className="btn btn-block btn-success">Submit</button>
                    <br/>
                    <Link to="/login"><p>Account Access</p></Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
