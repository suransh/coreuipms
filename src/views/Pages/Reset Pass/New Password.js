/**
 * Created by prabal on 28/6/17.
 */

import React from 'react';
import {withRouter,Link} from 'react-router-dom';
import classnames from 'classname';
import validateInput from '../../../utils/ValidatingForms';
import axios from 'axios';
import Loading from 'react-loading-animation';
import config from '../../../config';

class NewPassword extends React.Component {

    constructor() {
        super();
        this.state = {
            newPassword: "",
            confirmPassword: "",
            error: {},
            success: {},
            isLoading: false,
            isFetching:true
        }
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        const {isValid, error} = validateInput(this.state);
        if (!isValid) {
            this.setState({
                error: error
            });
        }
        else {
            this.setState({ error: {}, isLoading: true });
            const { id, confirmPassword } = this.state;
            axios.post('http://' + config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/setpass',{ id: id, confirmPassword: confirmPassword }).then ( (res) => {
                this.setState({ success: res.data.success, error: {}, isLoading:false });
            } ).catch( (err) => {
                this.setState({ isLoading: false });
                throw err;
            } )
        }

    }

    componentWillMount() {
        axios.post('http://'+ config.APP_SERVER_IP + ":" + config.APP_SERVER_PORT + '/api/verify',{token : this.props.match.params.token}).then( (res) => {
            this.setState({ error: res.data.error, id: res.data.id, isFetching:false });
        } ).catch( (err) => {
            throw err;
        } )
    }

    render() {
        const {error,isFetching} = this.state;
        if (isFetching) return <Loading/>;
        return (
            <div className="app flex-row align-items-center">
                <div className="container">
                    <div className="row justify-content-center">
                        { error.invalid ? this.errormsg(): this.form()}
                    </div>
                </div>
            </div>
        );
    }

    form() {
        const {success, error, isLoading} = this.state;
        return (
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                            <strong>Set New Password</strong>
                        </div>
                        <div className="card-block">
                            { success.form && <div className="alert alert-success">{success.form}</div>  }
                            { error.form && <div className="alert alert-danger">{error.form}</div>  }
                            <form action="" method="">
                                <div className={ classnames("form-group", {'has-danger' : error.newPassword} ) }>
                                    <label htmlFor="nf-password">New Password</label>
                                    <input type="password" id="nf-password" name="newPassword" onChange={this.onChange.bind(this)} className="form-control" placeholder="New Password.."/>
                                    {error.newPassword && <span className="help-block text-danger">{error.newPassword}</span>}
                                </div>
                                <div className={ classnames("form-group", {'has-danger' : error.confirmPassword} ) }>
                                    <label htmlFor="nf-password">Confirm Password</label>
                                    <input type="password" id="nf-password" name="confirmPassword" onChange={this.onChange.bind(this)} className="form-control" placeholder="Confirm Password.."/>
                                    {error.confirmPassword && <span className="help-block text-danger">{error.confirmPassword}</span>}
                                </div>
                            </form>
                        </div>
                        <div className="card-footer">
                            <button type="submit" onClick={this.onSubmit.bind(this)} disabled={isLoading} className="btn btn-sm btn-primary"><i className="fa fa-dot-circle-o"/> Submit</button>
                            <span> </span>
                            <Link to="/login" > <button className="btn btn-sm btn-success"> <i className="fa fa-user" /> Login </button> </Link>
                        </div>
                    </div>
                </div>
        );
    }

    errormsg() {
        const {error} = this.state;
        return (
            <span className="alert alert-danger">{error.invalid}</span>
        )
    }
}

export default withRouter(NewPassword);

