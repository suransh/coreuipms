/**
 * Created by prabal on 16/6/17.
 */


import { combineReducers } from 'redux';

import auth from './reducers/auth';

export default combineReducers ({
    auth
});