import React from 'react';
import {Route,Switch,Redirect} from 'react-router-dom';

// Containers
import Full from './containers/Full/'

// Views
import Login from './views/Pages/Login/'
import ResetPass from './views/Pages/Reset Pass/Reset Password'
import Page404 from './views/Pages/Page404/'
import Page500 from './views/Pages/Page500/'
import NewPass from './views/Pages/Reset Pass/New Password';

function checkAuth() {
    const token = localStorage.getItem('jwtToken');
    if (token) return true;
    return false;
}

function generateRoute(requireAuth,direct,redirect,comp) {
    if (requireAuth) {
        return (
            <Route exact path={direct} render={() => (
                !checkAuth() ? (
                    <Redirect to={redirect}/>
                ) : (
                    comp
                )
            )}/>
        )
    }
    else {
        return (
            <Route exact path={direct} render={() => (
                checkAuth() ? (
                    <Redirect to={redirect} />
                ) : (
                    comp
                )
            )}/>
        )
    }
}

class Routes extends React.Component {

    render() {
        return (
            <Switch >
                {generateRoute(false,'/login','/',<Login/>)}
                {generateRoute(false,'/reset','/',<ResetPass/>)}
                {generateRoute(false,'/reset/:token','/',<NewPass/>)}
                <Route exact path="/404" name="Page 404" component={Page404}/>
                <Route exact path="/500" name="Page 500" component={Page500}/>
                <Route path="/" render={() => (
                    !checkAuth() ? (
                        <Redirect to='/login' />
                    ) : (
                        <Full/>
                    )
                )}/>
            </Switch>
        )
    }
}

export default Routes;