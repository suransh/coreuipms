const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const expressValidator = require('express-validator');
const mongoose = require('mongoose');
const User = require('./Models/users');

// Load environment variables
require('dotenv').config();

//Body-Parser Middlware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());

//Connect to Database
const url = process.env.YOUR_MONGO_URL + '/pmsdb';
mongoose.connect(url);
const db = mongoose.connection;

//Adding Super Admin ( If does not exist )
User.addSuperAdmin(function (err) {
   if (err) throw err;
});

//Cross Origin Resource Sharing
const cors = require('cors');
app.use(cors());


//Routers
const UpdatePassword = require('./routes/UpdatePass');
const auth = require('./routes/auth');
const ResetPassword = require('./routes/ResetPass/ResetPass');
const VerifyToken = require('./routes/ResetPass/VerifyToken');
const SetPass = require('./routes/ResetPass/SetPass');
const EditProfile = require('./routes/EditProfile');
const admin = require('./routes/Admin/routes');
app.use('/api/auth',auth);
app.use('/api/up',UpdatePassword);
app.use('/api/reset',ResetPassword);
app.use('/api/verify',VerifyToken);
app.use('/api/setpass',SetPass);
app.use('/api/profile/edit',EditProfile);
app.use('/api/admin',admin);

//Setting up port and initialising server
const port = process.env.APP_SERVER_PORT || 8000;
app.listen(port, function(){
    console.log('Server started on port '+ port);
});