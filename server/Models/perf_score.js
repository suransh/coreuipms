const mongoose = require('mongoose');
const mongodb = require('mongodb');

const ScoreSchema = new mongoose.Schema({
    categoryId: {
        type: String,
    },
    score: {
        type: Number
    }
});

const ReqSchema = new mongoose.Schema({
        uid: {
            type: String,
            unique: true
        },
        year: {
            type: Number
        },
        week: {
            type: Number
        },
        perf: [ScoreSchema]
});

const perfscore = module.exports = mongoose.model('perfscore',ReqSchema);

function add(data,callback) {
    let doc = {
        uid: data.selectedId,
        year: data.selectedYear,
        week: data.selectedWeek,
        perf: []
    };
    let arr = [];
    for ( let [key,val] of Object.entries(data.score) ) {
        arr.push({ categoryId: key, score: val });
    }
    doc['perf'] = arr;
    let newDoc = new perfscore(doc);
    newDoc.save(callback);
}

function edit(doc,data,callback) {
    let arr = [];
    for (let [key,val] of Object.entries(data.score) ) {
        arr.push({ categoryId: key, score: val });
    }
    doc['perf'] = arr;
    doc.save(callback);
}

module.exports = {
    scoreEmployee: function (data, callback) {
        perfscore.findOne({ $and : [ { uid: data.selectedId }, { year: data.selectedYear },
            { week: data.selectedWeek } ] }, function (err,doc) {
            if (err) throw err;
            if (!doc) {
                add(data,callback);
            }
            else {
                edit(doc,data,callback);
            }
        } )
    },
    getScore: function (data, callback) {
        perfscore.findOne({ $and : [ { uid: data.selectedId }, { year: data.selectedYear },
            { week: data.selectedWeek } ] }, callback);
    }
};