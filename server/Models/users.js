/**
 * Created by prabal on 14/6/17.
 */

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const lodash = require('lodash');
const constants = require('../constants.json');

// User Schema
const UserSchema = mongoose.Schema({
    id : {
      type: String,
        index: true,
        unique: true
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    email: {
        type: String,
    },
    password: {
        type: String
    },
    active: {
        type: Boolean
    },
    role: {
        type: String
    }
});

UserSchema.index({'$**': 'text'});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports = {

    getUserByEmail: function (email, callback) {
        let query = {email: email};
        User.findOne(query, callback);
    },
    getUserByUid: function (uid, callback) {
        let query = {_id: uid};
        User.findOne(query, callback);
    },
    updatePassword: function (user, newPassword, callback) {
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(newPassword, salt, function (err, hash) {
                user.password = hash;
                user.save(callback);
            });
        });
    },

    comparePassword: function (candidatePassword, hash, callback) {
        bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
            if (err) throw err;
            callback(null, isMatch);
        });
    },

    editUser: function (user, newUser, callback) {
        for (let [key, val] of Object.entries(newUser)) {
            if (key === "id") continue;
            user[key] = val;
        }
        user.save(callback);
    },

    listUsers:{
        cnt: function (regex,filter,callback) {
            User.find( { $or : [ { id: { $regex: regex} },
                {firstName: { $regex: regex} },
                { lastName: { $regex: regex} },
                { email: { $regex: regex } },
                { role: {$regex: regex} }
            ],role: { $nin: filter } }).count(callback);
        },
        result: function(col, type, page, regex,filter, callback) {
                User.find( { $or : [ { id: { $regex: regex} },
                    {firstName: { $regex: regex} },
                    { lastName: { $regex: regex} },
                    { email: { $regex: regex } },
                    { role: {$regex: regex} }
                ],role: { $nin: filter }}).
                select('-password -__v').
                sort( (type==='DEC' ? '-' : '') + col).
                skip((page-1)*10).limit(10).exec(callback);
        }
    },
    
    addUser: function (user,callback) {
        user["password"] = "";
        let newUser = new User(user);
        newUser.save(callback);
    },

    removeUserbyUid: function (uid,callback) {
      User.findOne({_id:uid}).remove().exec(callback)
    },
    getEmpNames: function (callback) {
        User.find().select('_id firstName lastName role').exec(callback);
    },
    addSuperAdmin: function (callback) {
        User.findOne({ role: constants.role.superAdmin }, function (err,user) {
            if (err) throw err;
            if (!user) {
                let SuperAdmin = {
                    id: "0",
                    firstName: "Super",
                    lastName: "Admin",
                    email: "superadmin@xyz.com",
                    role: constants.role.superAdmin,
                    active: true,
                    password: "$2a$10$RinCTtzhN6pOJO7lKTV2ee/2g2AZKre0pqjHLozM5/AS6JOylaYDK"
                };
                let newUser = new User(SuperAdmin);
                newUser.save(callback);
            }
        })
    }
};