/**
 * Created by prabal on 14/7/17.
 */

const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
    name: String,
    weightage: Number,
    score: Number
});

//Required Schema
const ReqSchema = new mongoose.Schema({
   category: {
       name: String,
       weightage: Number
   },
    subCategory: [CategorySchema]
});

const categories = module.exports = mongoose.model('categories',ReqSchema);

module.exports = {
    addNewCategory: function (newCategory, callback) {
        let data = new categories({
            category: newCategory,
            subCategory: []
        });
        data.save(function (err) {
            if (err) throw err;
            callback();
        })
    },
    getCategories: function (callback) {
        categories.find(callback);
    },
    addSubCategory: function (id, subCategory, callback) {
        categories.update( { _id: id }, { $push: { subCategory: subCategory } }, callback );
    },
    deleteCategoryById: function (id, callback) {
        categories.findOne( { _id: id } ).remove().exec(callback);
    },
    editCategoryById: function (id, category, callback) {
        categories.update( { _id: id }, { $set: { category: category } }, callback  );
    },
    deleteSubCategoryById: function (categoryId, subCategoryId, callback) {
        categories.update( { _id: categoryId },{ $pull: { subCategory: { _id: subCategoryId } } }, callback  );
    },
    editSubCategory: function (categoryId, subCategoryId,newCategory, callback) {
        categories.update( {_id: categoryId, "subCategory._id":subCategoryId},
            { $set: { "subCategory.$": newCategory } }, callback);
    }
};