/**
 * Created by prabal on 21/6/17.
 */

const mongoose = require('mongoose');

// Required Schema
const ReqSchema = new mongoose.Schema({
    id : {
        type: String,
        index: true
    },
    email: {
        type: String,
        index:true
    },
    token: {
        type: String
    }
});

const resPassToken = module.exports = mongoose.model('resPassToken', ReqSchema);

module.exports.addToken = function (uid,email,token,callback) {
  let data = new resPassToken ({
      id: uid,
      email: email,
      token: token
  });
  data.save(function (err) {
      if (err) throw err;
      callback();
  })
};

module.exports.getUserByToken = function(token, callback){
    let query = {token: token};
    resPassToken.findOne(query, callback);
};

module.exports.getUserByUid = function(uid, callback){
    let query = {id: uid};
    resPassToken.findOne(query, callback);
};

module.exports.removeByUid = function (uid,callback) {
    resPassToken.remove( {id: uid}, function (err) {
        if (err) throw err;
        callback();
    } )
};

module.exports.updateToken = function(user,token, callback){
    user.token = token;
    user.save(callback);
};
