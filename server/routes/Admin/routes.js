/**
 * Created by prabal on 3/7/17.
 */
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const list = require('./Edit User/list');
const add = require('./Edit User/add');
const remove = require('./Edit User/remove');
const edit = require('./Edit User/edit');
const constants = require('../../constants.json');
const User = require('../../Models/users');
const categoryAdd = require('./Categories/add');
const categoryShow = require('./Categories/show');
const subCategoryAdd = require('./Categories/addSub');
const delCategory = require('./Categories/delCategory');
const editCategory = require('./Categories/editCategory');
const delSubCategory = require('./Categories/delSubCategory');
const editSubCategory = require('./Categories/editSubCategory');
const perfManageRouter = require('./Perf Management/routes');

function authenticateForAdmin(req,res,next) {
    const authorizationHeader = req.headers['authorization'];
    let token;

    if (authorizationHeader) {
        token = authorizationHeader.split(' ')[1];
    }

    if (token) {
        if (token) {
            jwt.verify(token,process.env.SECRET_KEY, (err,decoded) => {
                if (err) {
                    res.status(401).json({ error : "No such user" });
                }
                else {
                    const {role} = decoded;
                    if (role===constants.role.admin || role===constants.role.superAdmin) {
                        User.getUserByUid(decoded.uid, function (err,user) {
                          if (err) throw err;
                          res.locals = { user: user };
                          next();
                        })
                    }
                    else res.json({ error: "Access not Granted" })
                }
            })
        }
        else {
            res.status(403).json( { error: "No token provided" } );
        }
    }
}

router.use('/',authenticateForAdmin);
router.use('/list',list);
router.use('/add',add);
router.use('/remove',remove);
router.use('/edit',edit);
router.use('/category/add',categoryAdd);
router.use('/category/show',categoryShow);
router.use('/category/addSub', subCategoryAdd);
router.use('/category/delCategory',delCategory);
router.use('/category/editCategory',editCategory);
router.use('/category/delSubCategory',delSubCategory);
router.use('/category/editSubCategory',editSubCategory);
router.use('/perfmanage',perfManageRouter);

module.exports = router;

