const express = require('express');
const router = express.Router();
const perfScore = require('../../../Models/perf_score');

function getScore(req,res) {
    perfScore.getScore(req.query, function (err,perf) {
        if (err) throw err;
        res.json(perf);
    })
}

router.use('/',getScore);

module.exports = router;