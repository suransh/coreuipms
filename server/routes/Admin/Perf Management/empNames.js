
const express = require('express');
const router = express.Router();
const User = require('../../../Models/users');

function getEmpNames(req,res) {
    User.getEmpNames(function (err,data) {
        if (err) throw err;
        res.send(data);
    })
}

router.get('/',getEmpNames);

module.exports = router;