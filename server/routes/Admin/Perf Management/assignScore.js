const express = require('express');
const router = express.Router();
const perfscore = require('../../../Models/perf_score');

function assignScore(req, res) {
    perfscore.scoreEmployee(req.body, function (err) {
        if (err) throw err;
        res.send("Successfuly Done");
    });
}


router.use('/',assignScore);

module.exports = router;