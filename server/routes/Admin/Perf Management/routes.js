const express = require('express');
const router = express.Router();

const getEmpNames = require('./empNames');
const assignScore = require('./assignScore');
const getScore = require('./getScores');

router.use('/getname',getEmpNames);
router.use('/assignscore',assignScore);
router.use('/getscore',getScore);

module.exports = router;