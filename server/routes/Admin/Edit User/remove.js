/**
 * Created by prabal on 4/7/17.
 */

const express = require('express');
const router = express.Router();
const User = require('../../../Models/users');

function removeUser(req,res) {
    User.removeUserbyUid(req.body.id, function (err) {
        if (err) throw err;
        res.json({ success: "Removed Successdully" });
    })
}

router.delete('/',removeUser);

module.exports = router;
