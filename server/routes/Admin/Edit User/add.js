/**
 * Created by prabal on 4/7/17.
 */

const express = require('express');
const router = express.Router();
const User = require('../../../Models/users');
const resPassToken = require('../../../Models/ResetPasswordToken');
const ejs = require('ejs');
const nodemailer = require('nodemailer');
const randomToken = require('random-token');

//Server side Form Validation
function validate(req,res,next) {
    req.assert('firstName', "This field is required").notEmpty();
    req.assert('lastName', "This field is required").notEmpty();
    req.assert('email', "This field is required").notEmpty();
    req.assert('email', "Invalid Email Id").isEmail();
    req.assert('id', "This field is required").notEmpty();
    req.assert('role', "This field is required").notEmpty();

    req.getValidationResult().then(function (result) {

        if (!result.isEmpty()) {
            let error = {};
            const arr = result.array();
            for (let i = 0; i < arr.length; i++) {
                error[arr[i].param] = arr[i].msg;
            }
            res.json({ error: error, success: {} });
        } else {
            next();
        }
    });
}


function addUser(req,res) {
    let newUser = req.body;
    User.addUser(newUser, function (err,user) {
        if (err) throw err;
        res.json({ success: { form: "An Email to set password will be sent shortly" }, error: {} });
        addToken(user);
    })
}

function addToken(user) {
    resPassToken.getUserByUid(user._id, function (err,resPassUser) {
        if (err) throw err;

        const token = randomToken(32); //Generate a token
        let link = "http://"+process.env.CLIENT_SERVER_IP+ ":" +process.env.CLIENT_SERVER_PORT + "/reset/" + token;

        if (!resPassUser) {
            resPassToken.addToken(user._id,user.email,token, function (err) {
                if (err) throw err;
                sendMail(link,user.email);
            })
        }
        else {
            resPassToken.updateToken(resPassUser,token, function (err) {
                if (err) throw err;
                sendMail(link,resPassUser.email);
            })
        }
    })
}

function sendMail(link,email) {
    ejs.renderFile(__dirname + '/mail.ejs',{ direct:link }, function (err,str) {
        let transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            port: process.env.SMTP_PORT,
            auth: {
                user: process.env.SMTP_AUTH_USER,
                pass: process.env.SMTP_AUTH_PASS
            }
        });
        let mailOptions = {
            to : email,
            subject : "Set Password",
            html : str
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
        });
    })
}


router.post('/',validate,addUser);

module.exports = router;
