/**
 * Created by prabal on 3/7/17.
 */

const express = require('express');
const router = express.Router();
const authenticate = require('../../../Middlewares/authenticate');
const User = require('../../../Models/users');
const constants = require('../../../constants.json');

function listUsers(req,res) {
    let filter = [constants.role.superAdmin,constants.role.admin];
    const role = res.locals.user.role;
    if (role === constants.role.superAdmin) filter.pop();
    let regex = '.*';
    if (req.query.search) for ( let i = 0;i<req.query.search.length;i++) regex += req.query.search[i];
    regex += '.*';
    User.listUsers.cnt(regex,filter, function (err,count) {
            User.listUsers.result(req.query.col,req.query.type,req.query.page,regex,filter, function (err,result) {
                res.json({ result: result, count: count });
            })
    } )
}

router.get('/',listUsers);

module.exports = router;