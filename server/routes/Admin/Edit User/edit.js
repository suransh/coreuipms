/**
 * Created by prabal on 5/7/17.
 */

const express = require('express');
const router = express.Router();
const User = require('../../../Models/users');

//Server side Form Validation
function validate(req,res,next) {
    req.assert('user.firstName', "This field is required").notEmpty();
    req.assert('user.lastName', "This field is required").notEmpty();
    req.assert('user.email', "This field is required").notEmpty();
    req.assert('user.email', "Invalid Email Id").isEmail();
    req.assert('user.id', "This field is required").notEmpty();
    req.assert('user.role', "This field is required").notEmpty();

    req.getValidationResult().then(function (result) {

        if (!result.isEmpty()) {
            let error = {};
            const arr = result.array();
            for (let i = 0; i < arr.length; i++) {
                error[arr[i].param] = arr[i].msg;
            }
            res.json({ error: error, success: {} });
        } else {
            next();
        }
    });
}

function editUser(req,res) {
    User.getUserByUid(req.body.uid, function (err,user) {
        if (err) throw err;
        User.editUser(user,req.body.user, function (err) {
            if (err) throw err;
            res.json({ error: {}, success:{ form: "Changes Made Successfully" } });
        })
    })
}

router.use('/',validate,editUser);

module.exports = router;
