
const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function editSubCategory(req,res) {
    categories.editSubCategory(req.body.selectedId, req.body.toBeEdited, req.body.newCategory, function (err) {
        if (err) throw err;
        res.json({ success: "Edited Successfully" });
    });
}

router.put('/',editSubCategory);


module.exports = router;