const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function delSub(req,res) {
    categories.deleteSubCategoryById(req.body.selectedId,req.body.toBeDeleted, function (err) {
        if (err) throw err;
       res.send({ success: "Removed Successfully" });
    });
}

router.put('/',delSub);

module.exports = router;