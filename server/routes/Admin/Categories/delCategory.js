/**
 * Created by prabal on 18/7/17.
 */

const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function delCategory(req,res) {
    categories.deleteCategoryById(req.body.id, function (err) {
        if (err) throw err;
        res.json(" Removed Successfully ");
    })
}

router.delete('/',delCategory);

module.exports = router;
