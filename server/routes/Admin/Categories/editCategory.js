/**
 * Created by prabal on 18/7/17.
 */

const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function updateCategory(req,res) {
    categories.editCategoryById(req.body.id, req.body.category, function (err) {
        if (err) throw err;
        res.send({ success: "Successfully edited" });
    })
}

router.put('/',updateCategory);

module.exports = router;
