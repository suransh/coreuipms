/**
 * Created by prabal on 14/7/17.
 */

const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function addCategory(req,res) {
    categories.addNewCategory(req.body.newCategory, function (err, category) {
        if (err) throw err;
        res.send(category);
    })
}

router.post('/',addCategory);

module.exports = router;