/**
 * Created by prabal on 17/7/17.
 */

const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function getCategories(req,res) {
    categories.getCategories(function (err,data) {
        res.send(data);
    })
}

router.get('/',getCategories);

module.exports = router;
