/**
 * Created by prabal on 17/7/17.
 */

const express = require('express');
const router = express.Router();
const categories = require('../../../Models/categories');

function addSubCategory(req,res) {
    categories.addSubCategory(req.body.id, req.body.subCategory, function (err) {
        if (err) throw err;
        res.send("Done");
    })
}

router.post('/',addSubCategory);

module.exports = router;