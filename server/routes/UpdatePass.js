/**
 * Created by prabal on 20/6/17.
 */

const express = require('express');
const router = express.Router();
const validator = require('express-validator');
const util = require('util');
const User = require('../Models/users');
const authenticate = require('../Middlewares/authenticate');

//Server side Form Validation
function validate(req,res,next) {
    req.assert('oldPassword', "This field is required").notEmpty();
    req.assert('newPassword', "This field is required").notEmpty();
    req.assert('confirmPassword', "This field is required").notEmpty();
    req.assert('confirmPassword',"Passwords do not match").equals(req.body.newPassword);

    req.getValidationResult().then(function (result) {

        if (!result.isEmpty()) {
            let error = {};
            const arr = result.array();
            for (let i = 0; i < arr.length; i++) {
                error[arr[i].param] = arr[i].msg;
            }
            res.json({ error: error });
        } else {
            UpdatePassword(req,res);
        }
    });
}

function UpdatePassword(req,res) {
    User.getUserByUid(req.body.userId, function (err,user) {
        if (err) throw err;
        if (user) {
            User.comparePassword(req.body.oldPassword,user.password, function (err, IsMatch) {
                if (err) throw err;
                if (!IsMatch) {
                    res.json({ error: {form: "Incorrect Password" }, success:{} });
                }
                else {
                    User.updatePassword(user,req.body.newPassword, function (err) {
                        if (err) throw err;
                        res.json( { success: {form: "Password Updated Successfully" } , error: {} });
                    })
                }
            });
        }

    })
}

router.post('/',authenticate,validate);

module.exports = router;