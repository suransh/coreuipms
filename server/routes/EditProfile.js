/**
 * Created by prabal on 26/6/17.
 */

const express = require('express');
const router = express.Router();
const authenticate = require('../Middlewares/authenticate');
const User = require('../Models/users');

//Server side Form Validation
function validate(req,res,next) {
    req.assert('firstName', "This field is required").notEmpty();
    req.assert('lastName', "This field is required").notEmpty();
    req.assert('email', "This field is required").notEmpty();
    req.assert('email', "Invalid Email Id").isEmail();

    req.getValidationResult().then(function (result) {

        if (!result.isEmpty()) {
            let error = {};
            const arr = result.array();
            for (let i = 0; i < arr.length; i++) {
                error[arr[i].param] = arr[i].msg;
            }
            res.json({ error: error, success: {} });
        } else {
            next();
        }
    });
}

function editProfile(req,res) {
    User.getUserByUid(req.body.id, function (err,user) {
        if (err) throw err;
        else {
            User.editUser(user,req.body, function (err,user) {
                if (err) throw err;
                else
                {
                    let filteredUser = {
                        uid: user._id,
                        id: user.id,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        email: user.email,
                        role: user.role
                    };
                    res.json({ error: {}, success: { form : "Changes saved successfully" }, user: filteredUser });
                }
            })
        }
    })
}

router.post('/',authenticate,validate,editProfile);

module.exports = router;
