/**
 * Created by prabal on 22/6/17.
 */

const express = require('express');
const router = express.Router();
const User = require('../../Models/users');
const resPassToken = require('../../Models/ResetPasswordToken');

function changePassword(req,res) {
    User.getUserByUid(req.body.id, function (err,user) {
        if (err) throw err;
        User.updatePassword(user,req.body.confirmPassword,function (err) {
            if (err) throw err;
            resPassToken.removeByUid(req.body.id, function (err, user) {
              if (err) throw err;
                res.json({ error: {}, success: {form: "Password changed successfully"} });
            });
        })
    })
}

router.post('/',changePassword);

module.exports = router;
