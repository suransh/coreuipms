/**
 * Created by prabal on 21/6/17.
 */

const express = require('express');
const router = express.Router();
const validator = require('express-validator');
const util = require('util');
const randomToken = require('random-token');
const User = require('../../Models/users');
const resPassToken = require('../../Models/ResetPasswordToken');
const ejs = require('ejs');
const nodemailer = require('nodemailer');

//Server side Form Validation
function validate(req,res,next) {
    req.assert('email', "This field is required").notEmpty();
    req.assert('email', "Invalid Email Id").isEmail();

    req.getValidationResult().then(function (result) {

        if (!result.isEmpty()) {
            let error = {};
            const arr = result.array();
            for (let i = 0; i < arr.length; i++) {
                error[arr[i].param] = arr[i].msg;
            }
            res.json({ error: error });
        } else {
            next();
        }
    });
}

//Verify user by email
function verifyUser(req,res) {
    User.getUserByEmail(req.body.email, function (err,user) {
        if (err) throw err;
        if (!user) res.json({ error: { form: "Email Id does not exist" }, success: {} });
        else {
            resPassToken.getUserByUid(user._id, function (err,resPassUser) {
                if (err) throw err;

                const token = randomToken(32); //Generate a token
                let link = "http://"+process.env.CLIENT_SERVER_IP+ ":" +process.env.CLIENT_SERVER_PORT + "/reset/" + token;

                if (!resPassUser) {
                    resPassToken.addToken(user._id,user.email,token, function (err) {
                        if (err) throw err;
                        res.json({error: {}, success: {form: "Email will be sent shortly"} });
                        sendMail(link,user.email,res);
                    })
                }
                else {
                    resPassToken.updateToken(resPassUser,token, function (err) {
                        if (err) throw err;
                        res.json({error: {}, success: {form: "Email will be sent shortly"} });
                        sendMail(link,resPassUser.email,res);
                    })
                }
            })
        }
    })
}

function sendMail(link,email) {
    ejs.renderFile(__dirname + '/mail.ejs',{ direct:link }, function (err,str) {
    let transporter = nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        auth: {
            user: process.env.SMTP_AUTH_USER,
            pass: process.env.SMTP_AUTH_PASS
        }
    });
    let mailOptions = {
        to : email,
        subject : "Reset Password",
        html : str
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
    });
})
}

router.post('/',validate,verifyUser);

module.exports = router;
