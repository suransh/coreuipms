/**
 * Created by prabal on 22/6/17.
 */

const express = require('express');
const router = express.Router();

const resPassToken = require('../../Models/ResetPasswordToken');

function verifyToken(req,res) {
    resPassToken.getUserByToken(req.body.token, function (err,user) {
        if (err) throw err;
        if (!user) res.json({ error : { invalid: " Oops! This link has already been used or it does not exists " }, id: null });
        else {
            res.json({ error: {}, id: user.id });
        }
    })
}

router.post('/',verifyToken);


module.exports = router;
