/**
 * Created by prabal on 13/6/17.
 */
const express = require('express');
const router = express.Router();
const validator = require('express-validator');
const util = require('util');
const jwt = require('jsonwebtoken');
const User = require('../Models/users');

//Server side Form Validation
function validate(req,res,next) {
        req.assert('email', "This field is required").notEmpty();
        req.assert('password', "This field is required").notEmpty();
        req.assert('email', "Invalid Email Id").isEmail();

        req.getValidationResult().then(function (result) {

            if (!result.isEmpty()) {
                let error = {};
                const arr = result.array();
                for (let i = 0; i < arr.length; i++) {
                    error[arr[i].param] = arr[i].msg;
                }
                res.status(401).json({ error: error });
            } else {
                next();
            }
        });
}

function verifyUser(req,res) {
    User.getUserByEmail(req.body.email,function (err,user) {
            if (user) {
                User.comparePassword(req.body.password,user.password, function (err,IsMatch) {
                    if (err) throw err;
                    if (IsMatch) {

                        let UserFiltered = {
                            uid: user._id,
                            id: user.id,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            email: user.email,
                            role: user.role
                        };
                        const token = jwt.sign(UserFiltered, process.env.SECRET_KEY);
                        res.json({ token: token, error : {} });
                    } else {
                        res.status(401).json({ error: { form: 'Invalid Credentials' } });
                    }
                })
            } else {
                res.status(401).json({ error: { form: 'Invalid Credentials' } });
            }
        }
    )
}

router.post('/',validate,verifyUser);

module.exports = router;