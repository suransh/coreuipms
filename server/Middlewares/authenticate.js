/**
 * Created by prabal on 21/6/17.
 */

const jwt = require('jsonwebtoken');

module.exports =  function (req,res,next) {
    const authorizationHeader = req.headers['authorization'];
    let token;

    if (authorizationHeader) {
        token = authorizationHeader.split(' ')[1];
    }

    if (token) {
        jwt.verify(token,process.env.SECRET_KEY, (err,decoded) => {
            if (err) {
                res.status(401).json({ error : "No such user" });
            }
            else {
                next();
            }
        })
    }
    else {
        res.status(403).json( { error: "No token provided" } );
    }

};
