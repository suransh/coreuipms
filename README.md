This project is based on the [create-react-app template](https://github.com/facebookincubator/create-react-app/blob/master/template/README.md).
It also uses [CoreUI template for React](https://github.com/mrholek/CoreUI-Free-Bootstrap-Admin-Template/tree/master/React_Full_Project)

# Employee Performance Management System

# Generic Setup
## Installation Procedure
    1. Clone the repository https://suransh@bitbucket.org/suransh/coreuipms.git
    2. Go inside the `coreuipms` folder
    3. Install all dependancies using `npm install` (it will take some time, please be patient)
    4. Run the application using `npm start` command.
    5. Launch tests using `npm run test` command.

# Database 
This project uses [Mongo DB](https://docs.mongodb.com/?_ga=2.146445554.71991618.1501065034-1965024712.1501065034) for database. You can install it from [here](https://docs.mongodb.com/manual/installation)
## Setup
    - For Local DB : Start mongo db service using 'service mongod start'
    - For Third Party Service : Add your Mongo URL to .env file

# Server Setup
    1. Go inside the `server` folder
    2. Install all dependancies using `npm install`
    3. Run the application using `npm start` command.

# Environment Variables
All required environment variables are present in **server/.env.example** which includes:
    
- APP_SERVER_IP
- APP_SERVER_PORT
- CLIENT_SERVER_IP
- CLIENT_SERVER_PORT
- SECRET_KEY - A key required for json-web-token
- SMTP_HOST = Your SMTP host
- SMTP_PORT = Your SMTP Port
- SMTP_AUTH_USER = Your SMTP User Key
- SMTP_AUTH_PASS = Your SMTP Password
- YOUR_MONGO_URL = 'mongodb://localhost' or Your third party Mongo URL

After setting these variables rename the file to **.env** and start the server

# Note
Only Super Admin has the previledges to add or remove an employee. You can log in as a Super Admin using:
**Email : superadmin@xyz.com**
**Password : 1234**
